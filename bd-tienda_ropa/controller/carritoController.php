
<?php
include_once 'model/compras.php';

include_once 'model/articuloDAO.php';

class CarritoController {
    private $carrito;
    public function list() {
        $articulosEnCarrito = $this->carrito->obtenerArticulos();
        
      
        include_once 'views/cesta.php';
    }
    public function __construct() {
        session_start();
        if (!isset($_SESSION['carrito'])) {
            $_SESSION['carrito'] = new Carrito();
        }
        $this->carrito = $_SESSION['carrito'];
    }

    public function agregar() {
        if (isset($_GET['id'])) {
            $idarticulo = $_GET['id'];

            $articulo = ArticuloDAO::getArticuloByID($idarticulo);

            if ($articulo) {
                
                $this->carrito->agregarArticulo($articulo);
            }
        }

       
        header("Location: vistaCarrito.php");
    }

    public function quitar() {
        if (isset($_GET['id'])) {
            $idarticulo = $_GET['id'];
            $this->carrito->quitarArticulo($idarticulo);
        }
        header("Location: cesta.php");
    }

    public function mostrar() {
        $articulosEnCarrito = $this->carrito->obtenerArticulos();
        include_once 'views/cesta.php';
    }
}
?>

