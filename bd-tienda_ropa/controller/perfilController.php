<?php
include_once 'model/userDAO.php';

class PerfilController
{
    public function list() {
        // Verificar si se proporciona un ID de usuario en la URL
        if (isset($_GET['id'])) {
            $id_usuario = $_SESSION['id'];
            $usuario = UserDAO::getUserByID($id_usuario);

            // Verificar si el usuario existe
            if ($usuario) {
                // Verificar si se ha enviado el formulario de actualización
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    // Actualizar la información del usuario en la base de datos
                    $nombre = $_POST['nombre'];
                    $email = $_POST['email'];
                    $direccion = $_POST['direccion'];

                    UserDAO::edit($id_usuario, $nombre, $email, $usuario->getPassword(), $direccion);

                    // Puedes mostrar un mensaje de éxito aquí si lo deseas
                }

                // Incluir la vista y pasar la variable $usuario
                $view = 'views/perfil.php';
                include_once 'views/main.php';
            } else {
                // Manejar el caso en que el usuario no existe
                echo "El usuario no existe";
            }
        } else {
            // Manejar el caso en que no se proporciona un ID de usuario
            echo "ID de usuario no proporcionado";
        }
    }

    
}

    ?>