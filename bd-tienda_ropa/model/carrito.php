<?php
class Carrito {
    private $articulos;

    public function __construct() {
        $this->articulos = [];
    }

    public function agregarArticulo($articulo) {
        $this->articulos[] = $articulo;
    }

    public function obtenerArticulos() {
        return $this->articulos;
    }
}
?>