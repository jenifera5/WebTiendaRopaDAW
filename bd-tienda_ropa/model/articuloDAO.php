<?php


include_once 'config/database.php';
include_once 'articulo.php';


class ArticuloDAO
{


    public static function getAllArticulos()
    {
        
        $con = Database::connect();

        $stmt = $con->prepare("SELECT * FROM ARTICULOS");
        $stmt->execute();
        $result = $stmt->get_result();

        $listaarticulos = [];

        while ($articulo = $result->fetch_object('Articulo')) {
            $listaarticulos[] = $articulo;
        }

        return $listaarticulos;
    }
  public static function getArticuloByID($id)
    {
        $con = Database::connect();
        $stmt = $con->prepare("SELECT * FROM ARTICULOS WHERE idarticulos = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        $articulo = $result->fetch_object('Articulo');

        $con->close();

        return $articulo;
    }

    public static function add($nombre, $precio, $descripcion, $idcategoria, $img)
    {
        $con = Database::connect();
        $stmt = $con->prepare("INSERT INTO articulos (nombre, precio, descripcion, img, idcategoria) VALUES (?,?,?,?,?)");
        $stmt->bind_param("sdssi", $nombre, $precio, $descripcion, $img, $idcategoria);
        $stmt->execute();
        $con->close();
    }

    public static function edit($idarticulos, $nombre, $precio, $descripcion, $idcategoria, $img)
    {
        $con = Database::connect();
        $stmt = $con->prepare("UPDATE articulos SET nombre = ?, precio = ?, descripcion = ?, idcategoria = ?, img = ? WHERE idarticulos = ?");
        $stmt->bind_param("ssdsii", $nombre, $precio, $descripcion, $idcategoria, $img, $idarticulos);
        $stmt->execute();
        $con->close();
    }

    public static function delete($idarticulos)
    {
        $con = Database::connect();
        $stmt = $con->prepare("DELETE FROM articulos WHERE idarticulos = ?");
        $stmt->bind_param("i", $idarticulos);
        $stmt->execute();

        $con->close();
    }
    public static function obtenerPrecioPorId($idarticulos) {
        $con = Database::connect();
        $stmt = $con->prepare("SELECT precio FROM articulos WHERE idarticulos = ?");
        $stmt->bind_param("i", $idarticulos);
        $stmt->execute();
        $result = $stmt->get_result();

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $precio = $row['precio'];
            $con->close();
            return $precio;
        } else {
            $con->close();
            return false; // Devuelve false si no se encuentra el artículo con el ID dado
        }
    }

      
    
    
     
  
}
?>
