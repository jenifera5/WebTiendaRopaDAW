<div class="login-container">
    <div class="login-content">
        <div class="l-content">
            <div class="section-title">
                <h2 class="login-heading">Log In</h2>
                <p class="text">Introduce tus datos</p>
            </div>

            <form action="?controller=Perfil" method="post" class="form">
                <div class="input-group">
                    <label for="usuario" class="label">Usuario*</label>
                    <input type="text" name="usuario" class="input" placeholder="Tu Nombre" required>
                </div>

                <div class="input-group">
                    <label for="password" class="label">Contraseña*</label>
                    <input type="password" name="password" class="input" placeholder="Tu Contraseña" required>
                </div>

                <div class="buttons">
                    <button type="submit" class="button">Log In</button>
                    <button type="button" class="button-google">
                        <img src="img/google-icon.png" alt="Google Icon" class="google-icon">
                        Log In with Google
                    </button>
                </div>
            </form>

            <div class="footer-login">
                <p class="footer-text">Don't have an account? <a href="controller=Registro&action=showRegistroForm" class="footer-link">Sign up</a></p>
            </div>
        </div>
    </div>
</div>


