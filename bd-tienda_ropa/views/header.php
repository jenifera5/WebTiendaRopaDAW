
<nav class="navbar navbar-expand-sm navbar-dark bg-purple">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img src="img/logo.png" alt="Logo navbar"  width="80px"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
      
        
    </button>
    
      <ul class="navbar-nav me-auto">
       <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">Home</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="?controller=Login&action=showLoginForm">Login <i class="fa-solid fa-right-to-bracket"></i></a></li>
            <li><a class="dropdown-item" href="?controller=Registro&action=showRegistroForm">Registro <i class="fa-solid fa-address-card"></i></a></li>
          </ul>
        <li>
        <a class="nav-link" href="?controller=Carrito">Cesta <i class="fa-solid fa-cart-shopping"></i></a>
           
        </li>
        <li>
        <a class="nav-link" href="?controller=Articulo">Compra</a>
           
        </li>
        <li>
        <a class="nav-link" href="?controller=Perfil">perfil</a>
           
        </li>
        <ul class="navbar-nav d-flex">
       
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            User
          </a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="?controller=Dashboard">Admin</a></li>
          </ul>
        </li>
      </ul>
      </ul>
    
    </div>
    <form class="d-flex">
        <input class="form-control me-3" type="search" placeholder="Search">
        <button class="btn btn-primary" type="button">Search</button>
      </form>
      


</div> 
  </div>
</nav>
<div id="demo" class="carousel slide" data-bs-ride="carousel">

  <!-- Indicators/dots -->
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
    <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
    <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
  </div>
  
  <!-- The slideshow/carousel -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/la.jpg" alt="Los Angeles" class="d-block" style="width:100%">
      <div class="carousel-caption">
        <h3>Los Angeles</h3>
        <p>Aprovecha nuestras ofertas</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/chicago.jpg" alt="Chicago" class="d-block" style="width:100%">
      <div class="carousel-caption">
        <h3>Chicago</h3>
        <p>Aprovecha nuestras ofertas</p>
      </div> 
    </div>
    <div class="carousel-item">
      <img src="img/ny.jpg" alt="New York" class="d-block" style="width:100%">
      <div class="carousel-caption">
        <h3>New York</h3>
        <p>Aprovecha nuestras ofertas</p>
      </div>  
    </div>
  </div>
  
  <!-- Left and right controls/icons -->
  <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
    <span class="carousel-control-next-icon"></span>
  </button>
</div>
