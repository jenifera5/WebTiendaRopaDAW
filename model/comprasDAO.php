<?php
include_once 'config/database.php';
include_once 'compras.php';
class comprasDAO
{
    public static function getAllCompras()
    {
        
        $con = Database::connect();

        $stmt = $con->prepare("SELECT * FROM COMPRAS");
        $stmt->execute();
        $result = $stmt->get_result();

        $listacompras = [];

        while ($compras = $result->fetch_object('Compras')) {
            $listacompras[] = $compras;
        }

        return $listacompras;
    }
    public static function realizarCompra($datosCompra) {
        $con = Database::connect();
        
        // Aquí implementarías la lógica para guardar la compra en la base de datos
        // Esto podría involucrar varias consultas SQL, dependiendo de cómo esté estructurada tu base de datos

        $con->close();
        return true; // O devuelve false si la operación falla
    }

    public static function obtenerComprasPorUsuarioId($id_usuario) {
        $con = Database::connect();
        $stmt = $con->prepare("SELECT * FROM compras WHERE id_usuario= ?");
        $stmt->bind_param("i", $id_usuario);
        $stmt->execute();
        $result = $stmt->get_result();

        $compras = [];
        while ($compra = $result->fetch_object()) {
            $compras[] = $compra;
        }

        $con->close();
        return $compras;
    }
}
?>