<a href="?controller=Articulo&action=edit">Crear Articulo</a>
<div class="container mt-5 mb-3">

    <div class="row">
        <?php
        foreach ($listaarticulos as $articulo) {
        ?>

            <div class="col-3">
                <div class="card">
                    <img class="card-img-top" src="img/<?= $articulo->getImg() ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?= $articulo->getNombre() ?></h5>
                        <p class="card-text"><?= $articulo->getDescripcion() ?></p>
                    </div>

                    <div class="card-footer text-muted">
                        <a href="#" class="btn btn-primary"><?= $articulo->getPrecio() ?></a>
                        <a href="#" class="btn btn-secondary"><?= $articulo->agregarArticulo() ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>