
<div>
    <h2>Carrito de Compras</h2>
    
    <?php foreach ($articulosEnCarrito as $articulo) : ?>
        <div class="product">
            <div class="product-title"><?= $articulo->getNombre() ?></div>
            <div class="product-price"><?= $articulo->getPrecio() ?></div>
          
            <img src="<?= $articulo->getImagen() ?>" alt="Product Image">
        </div>
    <?php endforeach; ?>
</div>

<div class="order-summary">
    <div class="order-summary-title">Order summary</div>
    <div class="shipping">Shipping: <?= $costo_envio ?></div>
    <div class="tax">Tax: <?= $impuestos ?></div>
    <div class="precio_total">Total: <?= $precio_total ?></div>
    <div class="fecha_compra">Total: <?= $fecha_compra ?></div>
    <button class="continue-btn">Continue to payment</button>
</div>
<?php

?>