<?php
include_once 'model/userDAO.php';

class LoginController {
    
    public function showLoginForm() {
        $view = 'views/login.php';
        include_once 'views/main.php';
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];

            if (UserDAO::login($username, $password)) {
                session_start();
                $_SESSION['username'] = $username;

                // Redirigir al perfil del usuario después del inicio de sesión
                header("Location:".url."?controller=Perfil");
                exit();
            }
        }
    }
}

?>

