<?php
include_once 'model/userDAO.php';
include_once 'model/user.php'; // Asegúrate de incluir la clase User

class PerfilController {
    public function list() {
        session_start(); // Asegúrate de iniciar la sesión

        if (isset($_SESSION['id_usuario'])) {
            // Usuario ha iniciado sesión, obtener detalles del usuario
            $id_usuario = $_SESSION['id_usuario'];
            $usuario = UserDAO::getUserByID($id_usuario);
        } else {
            // Usuario invitado
            $usuario = new User();
            $usuario->setNombre("Invitado");
            $usuario->setEmail("correo@ejemplo.com");
            // Establecer otros atributos del objeto User según sea necesario
        }

        // Verificar si se ha enviado el formulario de actualización
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['id_usuario'])) {
            // Actualizar la información del usuario en la base de datos
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $direccion = $_POST['direccion'];

            // Asegúrate de que el método edit de UserDAO maneja correctamente la actualización
            UserDAO::edit($id_usuario, $nombre, $email, $usuario->getPassword(), $direccion);
        }

        // Incluir la vista y pasar la variable $usuario
        $view = 'views/perfil.php';
        include_once 'views/main.php';
    }
}
?>
