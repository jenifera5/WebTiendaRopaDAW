<?php
include_once 'model/userDAO.php';

class LoginController {
    
    public function showLoginForm() {
        $view = 'views/login.php';
        include_once 'views/main.php';
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
    
            $usuario = UserDAO::login($username, $password);
            if ($usuario) {
                session_start();
                $_SESSION['username'] = $usuario->getNombre();
                $_SESSION['id_usuario'] = $usuario->getIdusuario(); // Necesitas esta función o algo similar
        
                // Redirigir al perfil del usuario
                header("Location:?controller=Perfil" );
                exit();
            }
        }
    }
}

?>

