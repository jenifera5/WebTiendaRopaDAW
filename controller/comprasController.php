<?php
include_once 'model/comprasDAO.php';
include_once 'model/carrito.php';

class ComprasController {
    private $carrito;

    public function __construct() {
        session_start();
        $this->carrito = $_SESSION['carrito'] ?? new Carrito();
    }

    public function procesarCompra() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $datosCompra = [
                'usuario_id' => $_SESSION['usuario_id'], // Asegúrate de tener esta información disponible
                'articulos' => $this->carrito->obtenerArticulos(),
                'total' => $_POST['total'], // Asegúrate de pasar el total de alguna manera
                // Otros datos relevantes para la compra
            ];
            $resultado = ComprasDAO::realizarCompra($datosCompra);
            if ($resultado) {
                // Limpia el carrito después de la compra
                unset($_SESSION['carrito']);
                header("Location: confirmacionCompra.php");
            } else {
                header("Location: errorCompra.php");
            }
        }
    }

    public function mostrarHistorialCompras() {
        $compras = ComprasDAO::obtenerComprasPorUsuarioId($_SESSION['id_usuario']);
        include_once 'vistas/historialCompras.php';
    }
}
?>
